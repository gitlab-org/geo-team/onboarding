## [First Name], Welcome to GitLab and to the Geo Team!

We are all excited that you are joining us on the [Geo Team](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/tenant-scale/geo/). You should have already received an onboarding issue from [The People Group](https://about.gitlab.com/handbook/people-group/) familiarizing yourself with GitLab, setting up accounts, accessing tools, etc. This onboarding issue is specific to the Geo Team people, processes and setup.

For the first week or so you should focus on your GitLab onboarding issue. There is a lot of information there, and the first week is very important to get your account information set up correctly. Tasks from this issue can start as you get ready to start contributing to the Geo team.

Much like your GitLab onboarding issue, each item is broken out by Owner: Action. Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### New Team Member

### Must Dos

* [ ] Open an access request (AR) to add new team member to `geo@gitlab.com` email list, `@geo-team` Slack group, `geo-be` Google group (might not need AR if already group manager). See [example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/11219). The manager can also choose to create this AR.
* [ ] Open AR for or add new team member to `group-geo` in GCP
* [ ] Open AR for Geo staging access. See this [AR](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/11221) for an example. The manager can also choose to create this AR.
* [ ] As part of your normal onboarding issue, if you've created a member page for yourself under `www-gitlab-com/data/team-members/person/`, please add the following `- Geo BE Team` under departments.  See example [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team_members/person/n/nsilva5.yml#L9)
* [ ] Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet your teammates
* [ ] Join these Slack channels: [#g_geo](https://gitlab.enterprise.slack.com/archives/C32LCGC1H), [#geo_lounge](https://gitlab.enterprise.slack.com/archives/C7U95P909), [#development](https://gitlab.enterprise.slack.com/archives/C02PF508L), [#backend](https://gitlab.enterprise.slack.com/archives/C8HG8D9MY) / [#frontend](https://gitlab.enterprise.slack.com/archives/C0GQHHPGW), [#engineering-fyi](https://gitlab.enterprise.slack.com/archives/CJWA4E9UG), [#company-fyi](https://gitlab.enterprise.slack.com/archives/C010XFJFTHN), [#company-fyi-private](https://gitlab.enterprise.slack.com/archives/C02E7JGBU4E), [#is-this-known](https://gitlab.enterprise.slack.com/archives/CETG54GQ0), [#core-development](https://gitlab.enterprise.slack.com/archives/CG7FPF4KT), [#s_tenant_scale](https://gitlab.enterprise.slack.com/archives/C07TWC3QX47), [#whats-happening-at-gitlab](https://gitlab.enterprise.slack.com/archives/C0259241C)
* [ ] Familiarize yourself with the team boards
  * [ ] [Build Board](https://gitlab.com/groups/gitlab-org/-/boards/1181257?label_name[]=group%3A%3Ageo&milestone_title=Started)
  * [ ] [Team Member](https://gitlab.com/groups/gitlab-org/-/boards/878547) visualizes issues assigned to each team member. Add yourself to this board if needed.
  * [ ] [Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1181258?label_name[]=group%3A%3Ageo)
* [ ] Link to relevant Gitlab repo Geo code
  * [ ] [General Geo Code](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/lib/gitlab/geo)
  * [ ] [Geo Models](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/app/models/geo)
  * [ ] [Geo Workers](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/app/workers/geo)
  * [ ] [Rake Tasks](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/tasks/geo.rake)
* [ ] Add the Geo Team calendar `gitlab.com_iil929sjtsnbaki2i58dtidh5s@group.calendar.google.com` to your Google calendar as well as Time Off By Deel (Under Calendar Sync, Additional Calendars to include). This will help you see the team's OOO as well as add your OOOs to the team calendar. 
* [ ] Read up about the team announcements [here](https://docs.google.com/document/d/1D17_eZBoQ2HnksKm-JIACD0qrRSo-RwQUXX-FhSMccg/edit#heading=h.j75sqcicg1o9) or follow the weekly slack announcements pinned in the `#geo-lounge` channel and in the canvas there
* [ ] Read about your team, its mission, team members and resources on the [Geo Team page](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/tenant-scale/geo/)
* [ ] Discuss with your manager your first issue assignment and work through it with support of your onboarding buddy

#### Team and Product

* [ ] Read about the [Geo product vision](https://about.gitlab.com/direction/geo/)
* [ ] Read about the product [workflow](https://handbook.gitlab.com/handbook/product-development-flow/)
* [ ] Read about how GitLab uses [labels](https://docs.gitlab.com/ee/user/project/labels.html)
* [ ] Watch recordings of demoes and discussions about Geo
  * Browse the Geo Team [Drive](https://drive.google.com/drive/folders/0AJRN6_TzCB8VUk9PVA) for recordings of past Office Hours/Geo Huddles/Demos  
  * [Geo demo for Dedicated team: Why use Geo?](https://youtu.be/tMWyKTtBwLE?t=544) ([private; log in as GitLab Unfiltered to view](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube))
  * [Geo demo for Dedicated team: Failovers](https://www.youtube.com/watch?v=jUkYsgAnFcc) ([private; log in as GitLab Unfiltered to view](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube))
  * [Geo PostgreSQL and DB clustering deep dive](https://www.youtube.com/watch?v=1xgG5nvwMGI)
  * [Geo Secondary proxying discussion](https://www.youtube.com/watch?v=DQDkjwB3dx0)
  * [Geo Self-service framework discussion](https://www.youtube.com/watch?v=8ejkmb6Bsbk)
* [ ] Familiarize yourself with the Geo administration docs and setup process
  * [ ] [How it works](https://docs.gitlab.com/ee/administration/geo/#how-it-works) and the [Architecture](https://docs.gitlab.com/ee/administration/geo/#architecture) 
  * [ ] [Replication Data Types](https://docs.gitlab.com/ee/administration/geo/replication/datatypes.html)
  * [ ] [Database Replication](https://docs.gitlab.com/ee/administration/geo/setup/database.html)
  * [ ] [Disaster Recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/)
  * [ ] [Create your own individual GCP account](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project)
* [ ] Read up on [Postgres AI](https://gitlab.com/-/snippets/2047960)
* [ ] Read up on [Teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Database_Console_via_Teleport.md)
* [ ] Link to relevant [Omnibus Geo code](https://gitlab.com/gitlab-org/omnibus-gitlab/-/tree/master/files/gitlab-ctl-commands-ee)

#### After 2-3 Months

* [ ] Set up Geo on GCP with a single node following the [docs](https://docs.gitlab.com/ee/administration/geo/setup/two_single_node_sites.html). Make an MR to update any outdated documentation.
* [ ] Perform a failover with your GCP Geo setup following the [docs](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/planned_failover.html).
* [ ] Set a reminder to add yourself as a [trainee maintainer](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer) (Senior Engineer+) or code reviewer (Intermediate Engineer) for GitLab after working here for 3 months. You can do this by adding the appropriate entries to the [team page entry file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) file with which you added yourself to the team page. Here are [examples for frontend](https://gitlab.com/search?search=%22gitlab%3A+reviewer+frontend%22&nav_source=navbar&project_id=7764&group_id=6543&search_code=true&repository_ref=master), and [examples for backend](https://gitlab.com/search?search=%22gitlab%3A+reviewer+backend%22&nav_source=navbar&project_id=7764&group_id=6543&search_code=true&repository_ref=master). Doing reviews is a good way to help other team members, improve GitLab, and learn more about the code base. Because we use [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) once you add yourself as a reviewer, people will assign merge requests for you to review.
* [ ] Join PM on at least one customer call
* [ ] Once you've settled into the team and project in the first few weeks, join the support team on customer calls for a few weeks to understand how Geo is used in the real world. Ping the support team in #g_geo and your manager(s) to let them know that you'd like to shadow these calls. After a few calls, consider writing about your experience talking to the customers, or use your experience to improve documentation that is often used by them.
* [ ] Join the refinement process (Phase 2) by letting your EM know you're ready to start helping refine issues in the backlog (this should probably be around the 2 - 3 month period)
* [ ] Read up about how to set up Geo with [GitLab Environment Toolkit (GET)](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#google-cloud-platform-gcp)
* [ ] Once you've settled in for the first 3+ months, shadow someone and then eventually join the customer support rotations when comfortable enough to do so. [Process](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/geo/process/#engineering-customersupport-rotation-process-trial-phase)
* [ ] Read about Geo on staging.gitlab.com
  * [ ] [Geo on staging handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/tenant-scale/geo/staging/)
  * [ ] [Geo on staging blog post](https://about.gitlab.com/blog/2020/04/16/geo-is-available-on-staging-for-gitlab-com/)
* [ ] Add yourself to [CODEOWNERS](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/CODEOWNERS) when ready. This allows you to enter the Geo Reviewer Roulette
* [ ] Read [Geo Development Documentation](https://docs.gitlab.com/ee/development/geo.html)
* [ ] [Set up GDK for Geo](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/geo.md) and apply for an Ultimate [license](https://handbook.gitlab.com/handbook/engineering/developer-onboarding/#working-on-gitlab-ee-developer-licenses) Leave all the default settings when creating the license. This will create a Gitlab issue for you to get a license.
* [ ] Take a look at the GET runner created by Ian - https://gitlab.com/ibaum/get-runner if you are planning to use GET to setup your environment
* [ ] Add yourself to staging ref [here](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/staging-ref/#admin-access) and use this [page](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/staging-ref/#request-access-to-gcp-project-and-environment) to get access to the GCP Project

### Manager

* [ ] Invite team member to weekly scheduling call
* [ ] Add new team member to [/gitlab-org/geo-team](https://gitlab.com/gitlab-org/geo-team), [/gitlab-org/geo-team/geo-backend](https://gitlab.com/gitlab-org/geo-team/geo-backend)
* [ ] Add new team member to [Geekbot weekly standup](https://app.geekbot.com/dashboard/)
* [ ] Add new team member to geo@gitlab.com by managing members at https://groups.google.com/a/gitlab.com/g/geo
* [ ] Introduce new team member in `#g_geo`, `#geo_lounge`, `#development`, `#s_tenant_scale`
* [ ] Work with PM and onboarding buddy to assign 1-2 "Good First Issues" and fill in above
* [ ] Add new team member to the Geo Team calendar for meetings as well as to see other team member OOO/holidays
* [ ] Add new team member to [Geo Team Google Drive](https://drive.google.com/drive/folders/0AJRN6_TzCB8VUk9PVA)
* [ ] Add new team member to the [Geo Member Board](https://gitlab.com/groups/gitlab-org/-/boards/878547)
* [ ] Add new team member to the [Tenant Scale Group](https://gitlab.com/groups/gitlab-com/gl-infra/tenant-scale/geo/-/group_members?with_inherited_permissions=exclude)
