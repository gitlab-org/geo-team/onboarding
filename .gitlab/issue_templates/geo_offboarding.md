## Manager Tasks

- [ ] Remove from Geekbot standup
- [ ] Remove from /geo-team group
- [ ] Update Handbook team member .yml file
- [ ] Remove from CODEOWNERS https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/CODEOWNERS
- [ ] Access Requests:
  - [ ] Removal from geo@gitlab.com alias
  - [ ] Removal from @geo-team Slack group
  - [ ] Remove access for Geo on staging.
