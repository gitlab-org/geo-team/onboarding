# Geo Team Onboarding

This project is primarily an issue tracker for new team member onboarding. These are tasks specific to the Geo team. New team members should complete the general People Group and Engineer-specific onboarding issues first.